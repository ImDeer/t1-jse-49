package t1.dkhrunina.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.dto.ITaskDtoRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.dto.ITaskDtoService;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.exception.entity.TaskNotFoundException;
import t1.dkhrunina.tm.exception.field.*;
import t1.dkhrunina.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO, ITaskDtoRepository>
        implements ITaskDtoService {

    public TaskDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected ITaskDtoRepository getRepository(@NotNull EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDTO create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable final List<TaskDTO> tasks;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            tasks = repository.findAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    @SuppressWarnings("rawtypes, unchecked")
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<TaskDTO> models;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            models = repository.findAll(userId, comparator);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<TaskDTO> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

}