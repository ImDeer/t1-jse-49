package t1.dkhrunina.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.dto.IProjectDtoRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.dto.IProjectDtoService;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.exception.entity.ProjectNotFoundException;
import t1.dkhrunina.tm.exception.field.*;
import t1.dkhrunina.tm.repository.dto.ProjectDtoRepository;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO, IProjectDtoRepository>
        implements IProjectDtoService {

    public ProjectDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @Nullable
    @Override
    @SuppressWarnings("rawtypes, unchecked")
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<ProjectDTO> models;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            models = repository.findAll(userId, comparator);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }

        return models;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<ProjectDTO> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @NotNull
    @Override
    public ProjectDTO updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

}