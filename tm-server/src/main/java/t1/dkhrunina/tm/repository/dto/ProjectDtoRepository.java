package t1.dkhrunina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.repository.dto.IProjectDtoRepository;
import t1.dkhrunina.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO>
        implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<ProjectDTO> getEntityClass() {
        return ProjectDTO.class;
    }

}