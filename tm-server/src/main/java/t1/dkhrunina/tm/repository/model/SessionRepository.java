package t1.dkhrunina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.repository.model.ISessionRepository;
import t1.dkhrunina.tm.model.Session;

import javax.persistence.EntityManager;

public final class SessionRepository extends AbstractUserOwnedRepository<Session>
        implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<Session> getEntityClass() {
        return Session.class;
    }

}