package t1.dkhrunina.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.UserDTO;

public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

}