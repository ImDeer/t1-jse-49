package t1.dkhrunina.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    void clear();

    boolean existsById(@Nullable String id);

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    int getSize();

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@Nullable String id);

    @Nullable
    M removeByIndex(@Nullable Integer index);

    void update(@NotNull M model);

}