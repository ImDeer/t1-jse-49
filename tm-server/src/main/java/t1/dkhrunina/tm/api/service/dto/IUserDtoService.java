package t1.dkhrunina.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.enumerated.Role;

import java.util.Collection;
import java.util.List;

public interface IUserDtoService extends IDtoService<UserDTO> {

    @NotNull
    UserDTO add(@NotNull UserDTO user);

    void clear();

    @NotNull
    UserDTO remove(@Nullable UserDTO user);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    boolean existsById(@Nullable String id);

    List<UserDTO> findAll();

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    UserDTO findByLogin(@Nullable String login);

    UserDTO findOneById(@Nullable String id);

    UserDTO findOneByIndex(@Nullable Integer index);

    int getSize();

    UserDTO findByEmail(@Nullable String email);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    UserDTO removeById(@Nullable String id);

    UserDTO removeByIndex(@Nullable Integer index);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

    Collection<UserDTO> set(@NotNull Collection<UserDTO> users);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    void update(@NotNull UserDTO user);

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName,
                       @Nullable String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}