package t1.dkhrunina.tm.api.repository.dto;

import t1.dkhrunina.tm.dto.model.SessionDTO;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {

}