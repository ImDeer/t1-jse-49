package t1.dkhrunina.tm.migration;

import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import t1.dkhrunina.tm.AbstractSchemeTest;
import t1.dkhrunina.tm.api.repository.dto.IUserDtoRepository;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.repository.dto.UserDtoRepository;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import javax.persistence.EntityManager;

public class UserSchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        liquibase.update("user");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        @Nullable final UserDTO userDTO = createUser();
        Assert.assertNotNull(userDTO);
        Assert.assertEquals(1, getCountOfUsers());
        deleteProject(userDTO);
    }

    @NotNull
    private UserDTO createUser() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserDtoRepository userDtoRepository = new UserDtoRepository(entityManager);
        @NotNull final UserDTO userDTO = new UserDTO();
        userDtoRepository.add(userDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
        return userDTO;
    }

    private int getCountOfUsers() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserDtoRepository userDtoRepository = new UserDtoRepository(entityManager);
        final int count = userDtoRepository.getSize();
        entityManager.getTransaction().commit();
        entityManager.close();
        return count;
    }

    private void deleteProject(@NotNull final UserDTO userDTO) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserDtoRepository userDtoRepository = new UserDtoRepository(entityManager);
        userDtoRepository.remove(userDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}