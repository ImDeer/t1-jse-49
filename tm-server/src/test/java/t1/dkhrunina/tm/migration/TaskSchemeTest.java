package t1.dkhrunina.tm.migration;

import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import t1.dkhrunina.tm.AbstractSchemeTest;
import t1.dkhrunina.tm.api.repository.dto.ITaskDtoRepository;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.repository.dto.TaskDtoRepository;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import javax.persistence.EntityManager;

public class TaskSchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        liquibase.update("task");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        @Nullable final TaskDTO taskDTO = createTask();
        Assert.assertNotNull(taskDTO);
        Assert.assertEquals(1, getCountOfTasks());
        deleteProject(taskDTO);
    }

    @NotNull
    private TaskDTO createTask() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskDtoRepository taskDtoRepository = new TaskDtoRepository(entityManager);
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDtoRepository.add(taskDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
        return taskDTO;
    }

    private int getCountOfTasks() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskDtoRepository taskDtoRepository = new TaskDtoRepository(entityManager);
        final int count = taskDtoRepository.getSize();
        entityManager.getTransaction().commit();
        entityManager.close();
        return count;
    }

    private void deleteProject(@NotNull final TaskDTO taskDTO) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskDtoRepository taskDtoRepository = new TaskDtoRepository(entityManager);
        taskDtoRepository.remove(taskDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}