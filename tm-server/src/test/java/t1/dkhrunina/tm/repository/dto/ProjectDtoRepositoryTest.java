package t1.dkhrunina.tm.repository.dto;

import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.AbstractSchemeTest;
import t1.dkhrunina.tm.api.repository.dto.IProjectDtoRepository;
import t1.dkhrunina.tm.api.repository.dto.IUserDtoRepository;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectDtoRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private static EntityManager entityManager;

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private String USER1_ID;

    @NotNull
    private String USER2_ID;

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private IProjectDtoRepository projectRepository;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
        projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final UserDTO user1 = new UserDTO();
        USER1_ID = user1.getId();
        @NotNull final UserDTO user2 = new UserDTO();
        USER2_ID = user2.getId();
        userRepository.add(user1);
        userRepository.add(user2);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("project " + i);
            project.setDescription("description " + i);
            if (i < 5) project.setUserId(USER1_ID);
            else project.setUserId(USER2_ID);
            projectList.add(project);
            projectRepository.add(project);
        }
    }

    @After
    public void afterTest() throws InterruptedException {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final String projectName = "name";
        @NotNull final String projectDescription = "descr";
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(projectName);
        project.setDescription(projectDescription);
        @Nullable final ProjectDTO createdProject = projectRepository.add(USER1_ID, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(USER1_ID, createdProject.getUserId());
        Assert.assertEquals(projectName, createdProject.getName());
        Assert.assertEquals(projectDescription, createdProject.getDescription());
    }

    @Test
    public void testAddNull() {
        @Nullable final ProjectDTO createdProjectDTO = projectRepository.add(USER1_ID, null);
        Assert.assertNull(createdProjectDTO);
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = projectRepository.getSize() + 2;
        @NotNull final List<ProjectDTO> projects = new ArrayList<>();
        @NotNull final String firstProjectDTOName = "pdto name 1";
        @NotNull final String firstProjectDTODescription = "pdto descr 1";
        @NotNull final ProjectDTO firstProjectDTO = new ProjectDTO();
        firstProjectDTO.setName(firstProjectDTOName);
        firstProjectDTO.setDescription(firstProjectDTODescription);
        firstProjectDTO.setUserId(USER1_ID);
        projects.add(firstProjectDTO);
        @NotNull final String secondProjectDTOName = "pdto name 2";
        @NotNull final String secondProjectDTODescription = "pdto descr 2";
        @NotNull final ProjectDTO secondProjectDTO = new ProjectDTO();
        secondProjectDTO.setName(secondProjectDTOName);
        secondProjectDTO.setDescription(secondProjectDTODescription);
        secondProjectDTO.setUserId(USER1_ID);
        projects.add(secondProjectDTO);
        @NotNull final Collection<ProjectDTO> addedProjectDTOs = projectRepository.add(projects);
        Assert.assertTrue(addedProjectDTOs.size() > 0);
        int actualNumberOfEntries = projectRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void testClearForUser() throws Exception {
        int expectedNumberOfEntries = 0;
        projectRepository.clear(USER1_ID);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER1_ID));
    }

    @Test
    public void testFindAll() {
        @NotNull int allProjectDTOsSize = projectRepository.findAll().size();
        @Nullable final List<ProjectDTO> projects = projectRepository.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(allProjectDTOsSize, projects.size());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull Comparator<ProjectDTO> comparator = Sort.BY_NAME.getComparator();
        int allProjectDTOsSize = projectRepository.findAll().size();
        @Nullable List<ProjectDTO> projects = projectRepository.findAll(comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(allProjectDTOsSize, projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(allProjectDTOsSize, projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(allProjectDTOsSize, projects.size());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull List<ProjectDTO> projectListForUser = projectList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable final List<ProjectDTO> projects = projectRepository.findAll(USER1_ID);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull final List<ProjectDTO> projectListForUser = projectList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull Comparator<ProjectDTO> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<ProjectDTO> projects = projectRepository.findAll(USER1_ID, comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(USER1_ID, comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(USER1_ID, comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
    }

    @Test
    public void testFindOneById() {
        @Nullable ProjectDTO project;
        for (int i = 1; i <= projectList.size(); i++) {
            project = projectList.get(i - 1);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final ProjectDTO foundProjectDTO = projectRepository.findOneById(projectId);
            Assert.assertNotNull(foundProjectDTO);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final ProjectDTO foundProjectDTO = projectRepository.findOneById("meow");
        Assert.assertNull(foundProjectDTO);
        @Nullable final ProjectDTO foundProjectDTONull = projectRepository.findOneById(null);
        Assert.assertNull(foundProjectDTONull);
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull List<ProjectDTO> projectListForUser = projectList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final ProjectDTO project : projectListForUser) {
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final ProjectDTO foundProjectDTO = projectRepository.findOneById(USER1_ID, projectId);
            Assert.assertNotNull(foundProjectDTO);
        }
    }

    @Test
    public void testFindOneByIdNullForUser() {
        @Nullable final ProjectDTO foundProjectDTO = projectRepository.findOneById(USER1_ID, "meow");
        Assert.assertNull(foundProjectDTO);
        @Nullable final ProjectDTO foundProjectDTONull = projectRepository.findOneById(USER1_ID, null);
        Assert.assertNull(foundProjectDTONull);
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 1; i <= projectList.size(); i++) {
            @Nullable final ProjectDTO project = projectRepository.findOneByIndex(i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void testFindOneByIndexNull() {
        @Nullable final ProjectDTO project = projectRepository.findOneByIndex(null);
        Assert.assertNull(project);
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull List<ProjectDTO> projectListForUser = projectList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= projectListForUser.size(); i++) {
            @Nullable final ProjectDTO project = projectRepository.findOneByIndex(USER1_ID, i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        @Nullable final ProjectDTO project = projectRepository.findOneByIndex(USER1_ID, null);
        Assert.assertNull(project);
    }

    @Test
    public void testGetSize() {
        int actualSize = projectRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .count();
        int actualSize = projectRepository.getSize(USER1_ID);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        @Nullable final ProjectDTO project = projectList.get(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @Nullable final ProjectDTO deletedProjectDTO = projectRepository.remove(project);
        Assert.assertNotNull(deletedProjectDTO);
        @Nullable final ProjectDTO deletedProjectDTOInRepository = projectRepository.findOneById(projectId);
        Assert.assertNull(deletedProjectDTOInRepository);
    }

    @Test
    public void testRemoveNull() {
        @Nullable final ProjectDTO project = projectRepository.remove(null);
        Assert.assertNull(project);
    }

}