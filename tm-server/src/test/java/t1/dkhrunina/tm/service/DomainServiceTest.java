package t1.dkhrunina.tm.service;

import liquibase.exception.LiquibaseException;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.AbstractSchemeTest;
import t1.dkhrunina.tm.api.service.IDomainService;
import t1.dkhrunina.tm.api.service.ILoggerService;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.api.service.IServiceLocator;
import t1.dkhrunina.tm.api.service.dto.*;
import t1.dkhrunina.tm.api.service.model.*;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.service.dto.*;
import t1.dkhrunina.tm.service.model.*;

import java.util.List;

public class DomainServiceTest extends AbstractSchemeTest {

    @NotNull
    private IServiceLocator serviceLocator;

    @NotNull
    private UserDTO user;

    @NotNull
    private UserDTO admin;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() {
        serviceLocator = new IServiceLocator() {

            @Getter
            @NotNull
            final IPropertyService propertyService = new PropertyService();

            @Getter
            @NotNull
            final IProjectDtoService projectDtoService = new ProjectDtoService(connectionService);

            @Getter
            @NotNull
            final IProjectService projectService = new ProjectService(connectionService);

            @Getter
            @NotNull
            final ITaskDtoService taskDtoService = new TaskDtoService(connectionService);

            @Getter
            @NotNull
            final ITaskService taskService = new TaskService(connectionService);

            @Getter
            @NotNull
            final IProjectTaskDtoService projectTaskDtoService = new ProjectTaskDtoService(connectionService);

            @Getter
            @NotNull
            final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

            @Getter
            @NotNull
            final IUserDtoService userDtoService = new UserDtoService(propertyService, connectionService, projectTaskDtoService);

            @Getter
            @NotNull
            final IUserService userService = new UserService(propertyService, connectionService, projectTaskService);

            @Getter
            @NotNull
            final ISessionDtoService sessionDtoService = new SessionDtoService(connectionService);

            @Getter
            @NotNull
            final IDomainService domainService = new DomainService(this);

            @Getter
            @NotNull
            final ISessionService sessionService = new SessionService(connectionService);

            @Getter
            @NotNull
            final IAuthDtoService authDtoService = new AuthDtoService(propertyService, userDtoService, sessionDtoService);

            @Getter
            @NotNull
            final IAuthService authService = new AuthService(propertyService, userService, sessionService);

            @Getter
            @NotNull
            final ILoggerService loggerService = new LoggerService(propertyService, connectionService);
        };

        user = serviceLocator.getUserDtoService().create("USER", "USER", "user@user.user");
        admin = serviceLocator.getUserDtoService().create("ADMIN", "ADMIN", "admin@admin.admin", Role.ADMIN);
        @NotNull final ProjectDTO project1 = serviceLocator.getProjectDtoService().create(user.getId(), "project 1", "user project 1");
        @NotNull final ProjectDTO project2 = serviceLocator.getProjectDtoService().create(user.getId(), "project 2", "user project 2");
        @NotNull final ProjectDTO project3 = serviceLocator.getProjectDtoService().create(admin.getId(), "project 3", "admin project");
        @NotNull final TaskDTO task1 = serviceLocator.getTaskDtoService().create(user.getId(), "task 1", "project 1 task");
        task1.setProjectId(project1.getId());
        serviceLocator.getTaskDtoService().update(task1);
        @NotNull final TaskDTO task2 = serviceLocator.getTaskDtoService().create(user.getId(), "task 2", "project 2 task");
        task2.setProjectId(project2.getId());
        serviceLocator.getTaskDtoService().update(task2);
        @NotNull final TaskDTO task3 = serviceLocator.getTaskDtoService().create(admin.getId(), "task 3", "project 3 task");
        task3.setProjectId(project3.getId());
        serviceLocator.getTaskDtoService().update(task3);
    }

    @After
    public void afterTest() {
        serviceLocator.getTaskDtoService().clear(user.getId());
        serviceLocator.getTaskDtoService().clear(admin.getId());
        serviceLocator.getProjectDtoService().clear(user.getId());
        serviceLocator.getProjectDtoService().clear(admin.getId());
        serviceLocator.getUserDtoService().remove(user);
        serviceLocator.getUserDtoService().remove(admin);
    }

    @Test
    public void testDataBackup() {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBackup();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataBackup();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    public void testDataBase64() {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBase64();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataBase64();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    public void testDataBinary() {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBinary();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataBinary();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    public void testDataJsonFasterXml() {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonFasterXml();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataJsonFasterXml();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    public void testDataJsonJaxB() {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonJaxB();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataJsonJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    public void testDataXmlFasterXml() {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXmlFasterXml();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataXmlFasterXml();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    public void testDataXmlJaxB() {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXmlJaxB();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataXmlJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    public void testDataYamlFasterXml() {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataYamlFasterXml();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataYamlFasterXml();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

}