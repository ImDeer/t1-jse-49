package t1.dkhrunina.tm.util;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import t1.dkhrunina.tm.marker.UnitCategory;

@Ignore
@Category(UnitCategory.class)
public class FormatUtilTest {

    @Test
    public void testFormatBytes() {
        long bytes = 1;
        @NotNull String result = "";
        for (int i = 0; i <= 5; i++) {
            result = FormatUtil.formatBytes(bytes);
            Assert.assertFalse(result.isEmpty());
            bytes = bytes * 1024;
        }
    }

}