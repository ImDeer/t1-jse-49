package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.marker.UnitCategory;

@Ignore
@Category(UnitCategory.class)
public class PropertyServiceTest {

    @NotNull
    private IPropertyService propertyService;

    @Before
    public void initService() {
        propertyService = new PropertyService();
    }

    @Test
    public void testGetApplicationConfig() {
        @Nullable final String value = propertyService.getApplicationConfig();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetApplicationLog() {
        @Nullable final String value = propertyService.getApplicationLog();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetApplicationName() {
        @Nullable final String value = propertyService.getApplicationName();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetApplicationVersion() {
        @Nullable final String value = propertyService.getApplicationVersion();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetAuthorEmail() {
        @Nullable final String value = propertyService.getAuthorEmail();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetAuthorName() {
        @Nullable final String value = propertyService.getAuthorName();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetGitBranch() {
        @Nullable final String value = propertyService.getGitBranch();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetGitCommitId() {
        @Nullable final String value = propertyService.getGitCommitId();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetGitCommitterName() {
        @Nullable final String value = propertyService.getGitCommitterName();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetGitCommitterEmail() {
        @Nullable final String value = propertyService.getGitCommitterEmail();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetGitCommitMessage() {
        @Nullable final String value = propertyService.getGitCommitMessage();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetGitCommitTime() {
        @Nullable final String value = propertyService.getGitCommitTime();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetServerPort() {
        @Nullable final String value = propertyService.getServerPort();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetServerHost() {
        @Nullable final String value = propertyService.getServerHost();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetSessionKey() {
        @Nullable final String value = propertyService.getSessionKey();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    public void testGetTimeout() {
        @Nullable final Integer value = propertyService.getSessionTimeout();
        Assert.assertNotNull(value);
    }

}