package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.task.TaskUpdateByIndexRequest;
import t1.dkhrunina.tm.dto.response.task.TaskUpdateByIndexResponse;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-update-by-index";

    @NotNull
    private static final String DESCRIPTION = "Update task by index.";

    @Override
    public void execute() {
        System.out.println("[Update task by index]");
        System.out.println("Enter index: ");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description (optional): ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(getToken(), index, name, description);
        @NotNull final TaskUpdateByIndexResponse response = getTaskEndpoint().updateTaskByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}