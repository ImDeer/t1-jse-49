package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataSaveBase64Request;

public final class DataSaveBase64Command extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-s-base64";

    @NotNull
    private static final String DESCRIPTION = "Save data to BASE64 file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().saveBase64Data(new DataSaveBase64Request(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}