package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataSaveYamlFasterXmlRequest;

public final class DataSaveYamlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-s-yaml-faster";

    @NotNull
    private static final String DESCRIPTION = "Save data to FasterXML YAML file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().saveYamlFasterXmlData(new DataSaveYamlFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}