package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataLoadBinaryRequest;

public final class DataLoadBinaryCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-l-bin";

    @NotNull
    private static final String DESCRIPTION = "Load data from binary file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().loadBinaryData(new DataLoadBinaryRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}