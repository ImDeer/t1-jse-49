package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataSaveJsonJaxBRequest;

public final class DataSaveJsonJaxBCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-s-json-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Save data to JaxB JSON file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().saveJsonJaxBData(new DataSaveJsonJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}