package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataLoadJsonJaxBRequest;

public final class DataLoadJsonJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-l-json-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Load data from JaxB JSON file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().loadJsonJaxBData(new DataLoadJsonJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}