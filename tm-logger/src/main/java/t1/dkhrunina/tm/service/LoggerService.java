package t1.dkhrunina.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.ILoggerService;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService implements ILoggerService {

    private final ObjectMapper objectMapper = new YAMLMapper();

    @Override
    @SneakyThrows
    public void log(@NotNull final String message) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        @NotNull final byte[] bytes = message.getBytes();
        @NotNull final String fileName = table + ".yaml";
        @NotNull final File file = new File(fileName);
        if (!file.exists()) file.createNewFile();
        Files.write(Paths.get(fileName), bytes, StandardOpenOption.APPEND);
    }

}