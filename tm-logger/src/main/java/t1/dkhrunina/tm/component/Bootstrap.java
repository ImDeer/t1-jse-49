package t1.dkhrunina.tm.component;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.IReceiverService;
import t1.dkhrunina.tm.listener.EntityListener;
import t1.dkhrunina.tm.service.ReceiverService;

public class Bootstrap {

    public void init() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new EntityListener());
    }

}