package t1.dkhrunina.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @Nullable
    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Status status : values()) {
            if (status.getDisplayName().equals(value)) return status;
        }
        return null;
    }

    @NotNull
    public static String[] toValues() {
        @NotNull final String[] values = new String[Status.values().length];
        int index = 0;
        for (@NotNull final Status status : Status.values()) {
            values[index] = status.getDisplayName();
            index++;
        }
        return values;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}