package t1.dkhrunina.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error: number is incorrect");
    }

    public NumberIncorrectException(final String value, Throwable cause) {
        super("Error: value \"" + value + "\" is incorrect");
    }

}