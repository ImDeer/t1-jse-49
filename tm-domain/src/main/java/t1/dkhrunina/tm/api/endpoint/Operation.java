package t1.dkhrunina.tm.api.endpoint;

import t1.dkhrunina.tm.dto.request.AbstractRequest;
import t1.dkhrunina.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}