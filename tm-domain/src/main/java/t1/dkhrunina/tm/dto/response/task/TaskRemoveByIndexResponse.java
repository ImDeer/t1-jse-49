package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public class TaskRemoveByIndexResponse extends AbstractResultResponse {

    @Nullable
    private TaskDTO task;

    public TaskRemoveByIndexResponse(@Nullable final TaskDTO task) {
        this.task = task;
    }

    public TaskRemoveByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}