package t1.dkhrunina.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.AbstractUserRequest;
import t1.dkhrunina.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIdRequest(
            @Nullable final String token,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        super(token);
        this.id = id;
        this.status = status;
    }

}