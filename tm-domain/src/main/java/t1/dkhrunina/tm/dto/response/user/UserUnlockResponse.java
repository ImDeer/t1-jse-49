package t1.dkhrunina.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class UserUnlockResponse extends AbstractResultResponse {

    public UserUnlockResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}