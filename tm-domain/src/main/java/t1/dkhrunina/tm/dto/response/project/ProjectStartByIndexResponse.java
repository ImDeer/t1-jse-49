package t1.dkhrunina.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public class ProjectStartByIndexResponse extends AbstractResultResponse {

    @Nullable
    private ProjectDTO project;

    public ProjectStartByIndexResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

    public ProjectStartByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}