package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public class TaskChangeStatusByIdResponse extends AbstractResultResponse {

    @NotNull
    private TaskDTO task;

    public TaskChangeStatusByIdResponse(@NotNull final TaskDTO task) {
        this.task = task;
    }

    public TaskChangeStatusByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}