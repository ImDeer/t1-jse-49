package t1.dkhrunina.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.ProjectDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResultResponse {

    @Nullable
    private List<ProjectDTO> projects;

    public ProjectListResponse(@Nullable final List<ProjectDTO> projects) {
        this.projects = projects;
    }

    public ProjectListResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}