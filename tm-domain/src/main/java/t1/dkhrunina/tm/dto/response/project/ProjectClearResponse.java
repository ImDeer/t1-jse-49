package t1.dkhrunina.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class ProjectClearResponse extends AbstractResultResponse {

    public ProjectClearResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}