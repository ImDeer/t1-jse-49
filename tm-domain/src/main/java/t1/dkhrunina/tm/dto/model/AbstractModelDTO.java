package t1.dkhrunina.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.constant.DBConst;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractModelDTO implements Serializable {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = DBConst.COLUMN_CREATED, nullable = false, updatable = false)
    protected Date created = new Date();

    @Id
    @NotNull
    @Column(name = DBConst.COLUMN_ID, length = 36, nullable = false, updatable = false)
    private String id = UUID.randomUUID().toString();

}