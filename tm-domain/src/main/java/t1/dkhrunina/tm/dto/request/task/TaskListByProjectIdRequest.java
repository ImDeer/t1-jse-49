package t1.dkhrunina.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskListByProjectIdRequest(
            @Nullable final String token,
            @Nullable final String projectId
    ) {
        super(token);
        this.projectId = projectId;
    }

}