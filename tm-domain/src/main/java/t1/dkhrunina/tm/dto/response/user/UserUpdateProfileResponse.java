package t1.dkhrunina.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public class UserUpdateProfileResponse extends AbstractResultResponse {

    @Nullable
    private UserDTO user;

    public UserUpdateProfileResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

    public UserUpdateProfileResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}